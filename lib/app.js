

var uid2 = require('uid2');
var _ = require('lodash');
var moment = require('moment');
var express = require('express');
var session = require('express-session');
var swig = require('swig');
var bodyParser = require('body-parser');
var db = require('./db');



moment.locale('de');


swig.setDefaults({
    cache: false
});

swig.setFilter('forumEntryDate', function(date){
    return moment(date).format('dddd, DD.MM.YYYY HH:mm');
});



var app = express();
app.engine('html', swig.renderFile);
app.set('views', process.cwd() + '/lib/views');
app.set('view engine', 'html');
app.set('view cache', false);
app.set('trust proxy', 1) ;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(session({
    name: 'lib.session',
    secret: '4c86a2393d8ab56e2f21dd516fcbfbc0aec3a2c3',
    resave: true,
    saveUninitialized: true,
    cookie: {
        secure: false,
        httpOnly: false
    }
}));
app.use(express.static(process.cwd() + '/pub'));
app.use(function(req, res, next){

    if(req.session.user) {
        res.locals.user = req.session.user;
    }
    next();
});

app.get('/', function(req, res){
    res.render('home');
});

app.get('/login', function(req, res) {
    res.render('login');
});

app.post('/login', function(req, res) {

    var error = null;
    var username = req.body.username;
    var password = req.body.password;

    if(!db.authenticateUser(username, password)) {
        error = "Benutzername oder Passwort ist falsch!";
        res.render('login', {
            error: error
        });
    } else {
        req.session.user = db.getUserByUsername(username);
        res.redirect('/');
    }
});

app.get('/forum', function(req, res) {

    var forumEntries = db.getForumEntries();

    res.render('forum', {
        forumEntries: forumEntries
    });
});

app.post('/forum', function(req, res) {

    var forumEntries = db.getForumEntries();
    var user = {
        username: 'anonymous',
        avatar: 'http://lorempixel.com/60/60/cats/7'
    };

    if(req.session.user) {
        user = req.session.user;
    }

    var text = req.body.text;
    if(_.isString(text)) {
        text = _.trim(text);
    }

    if(_.isString(text) && text.length > 0 &&
        forumEntries.length < 5) {

        db.addForumEntry({
            user: user,
            text: text,
            date: new Date().toISOString()
        });
    }

    res.redirect('/forum');
});

app.all('/forum-reset', function(req, res) {
    db.resetForum();
    res.redirect('/forum');
});

app.all('/logout', function(req, res) {
    req.session.destroy();
    res.redirect('/');
});

app.use(function(req, res){
    res.render('404');
});


var srv = null;
module.exports.start = function(options) {

    srv = app.listen(options.port);
    srv.on('listening', function() {
        console.log('Grundlagen IT-Sicherheit: XSS-Demo');
        console.log('Victim listening at ' + srv.address().address + ':' + srv.address().port);
        console.log('Go to http://localhost:' + srv.address().port);
    });
}