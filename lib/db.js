


var _ = require('lodash');
var crypto = require('crypto');



var users = {

    alice: {
        username: 'alice',
        password: '5e91579f083d98222f4c3b814ee61380be90c3ed5209b76e334e4b5c9882eac8',
        role: 'user',
        avatar: 'http://lorempixel.com/60/60/animals/7'
    },

    bob: {
        username: 'bob',
        password: '89ddecd47dc0a04a2eaa1f3a75410903e89e470a293e517ccbe4687b6f1aa99e',
        role: 'admin',
        avatar: 'http://lorempixel.com/60/60/animals/8'
    }
};


var defaultForumEntries = [

    {
        user: users.bob,
        text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
        date: new Date().toISOString()
    },

    {
        user: users.alice,
        text: "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.",
        date: new Date().toISOString()
    }
];

var forumEntries = _.clone(defaultForumEntries);

module.exports.authenticateUser = function(username, password) {
    return username in users &&
        users[username].password === this.hashPassword(password)
};

module.exports.getUserByUsername = function(username) {
    return users[username];
};

module.exports.hashPassword = function(password) {
    return crypto.createHash('sha256').update(password).digest('hex');
};

module.exports.addForumEntry = function(entry) {
    forumEntries.push(entry);
};

module.exports.getForumEntries = function() {
    return forumEntries;
};

module.exports.resetForum = function() {
    return forumEntries = _.clone(defaultForumEntries);
};