

$(function() {

    $('.btn-login').click(function(event){
        event.preventDefault();
        document.location.href = "/login";
    });

    $('.btn-logout').click(function(event){
        event.preventDefault();
        document.location.href = "/logout";
    });
});